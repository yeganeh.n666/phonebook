package validators

import (
	"unicode"

	"github.com/go-playground/validator/v10"
)

// Validate validates user input, based on the given model.
func Validate(model interface{}) (bool, error) {
	validate := validator.New()
	validate.RegisterValidation("password", passwordValidator)
	err := validate.Struct(model)
	if err != nil {
		return false, err
	}
	return true, nil
}

/*
 * Password rules:
 * at least 7 letters
 * at least 1 number
 * at least 1 upper case
 * at least 1 special character
 */
func passwordValidator(fl validator.FieldLevel) bool {
	password := fl.Field().String()
	var (
		hasMinLen  = false
		hasUpper   = false
		hasLower   = false
		hasNumber  = false
		hasSpecial = false
	)
	if len(password) >= 7 {
		hasMinLen = true
	}
	for _, char := range password {
		switch {
		case unicode.IsUpper(char):
			hasUpper = true
		case unicode.IsLower(char):
			hasLower = true
		case unicode.IsNumber(char):
			hasNumber = true
		case unicode.IsPunct(char) || unicode.IsSymbol(char):
			hasSpecial = true
		}
	}
	return hasMinLen && hasUpper && hasLower && hasNumber && hasSpecial
}
