package jwt

import (
	"phonebook/internal/errors"

	"github.com/gin-gonic/gin"
)

// Handler returns a middleware that checks the access token.
func (a *Auth) Handler() gin.HandlerFunc {
	return func(c *gin.Context) {
		//Extract the access token metadata
		metadata, err := a.ExtractTokenMetadata(c.Request)
		if err != nil {
			c.Error(errors.Unauthorized(err.Error()))
			c.Abort()
			return
		}
		userid, err := a.FetchAuth(metadata)
		if err != nil {
			c.Error(errors.Unauthorized(err.Error()))
			c.Abort()
			return
		}
		c.Set("user_id", userid)
		c.Next()
		return
	}
}
