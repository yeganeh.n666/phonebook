package util

import (
	"net/http"
	"strconv"
)

var (
	// DefaultPageSize specifies the default page size
	DefaultPageSize = 10
	// MaxPageSize specifies the maximum page size
	MaxPageSize = 50
	// PageVar specifies the query parameter name for page number
	PageVar = "page"
	// PageLimitVar specifies the query parameter name for page size
	PageLimitVar = "limit"
)

// Pages represents a paginated list of data items.
type Pages struct {
	Page  int `json:"page"`
	Limit int `json:"limit"`
}

// New creates a new Pages instance.
// The page parameter is 1-based and refers to the current page index/number.
// The perPage parameter refers to the number of items on each page.
// And the total parameter specifies the total number of data items.
// If total is less than 0, it means total is unknown.
func New(page, limit int) *Pages {
	if limit <= 0 {
		limit = DefaultPageSize
	}
	if limit > MaxPageSize {
		limit = MaxPageSize
	}
	if page < 1 {
		page = 1
	}

	return &Pages{
		Page:  page,
		Limit: limit,
	}
}

// GetPaginationQueryParams creates a Pages object using the query parameters found in the given HTTP request.
// count stands for the total number of items. Use -1 if this is unknown.
func GetPaginationQueryParams(req *http.Request) *Pages {
	page := parseInt(req.URL.Query().Get(PageVar), 1)
	limit := parseInt(req.URL.Query().Get(PageLimitVar), DefaultPageSize)
	return New(page, limit)
}

// parseInt parses a string into an integer. If parsing is failed, defaultValue will be returned.
func parseInt(value string, defaultValue int) int {
	if value == "" {
		return defaultValue
	}
	if result, err := strconv.Atoi(value); err == nil {
		return result
	}
	return defaultValue
}
