package redis

import "github.com/go-redis/redis/v7"

// ConnectToRedis func connects to redis
func ConnectToRedis(dsn string) (*redis.Client, error) {
	opt, err := redis.ParseURL(dsn)
	if err != nil {
		return nil, err
	}
	client := redis.NewClient(opt)
	if _, err := client.Ping().Result(); err != nil {
		return nil, err
	}
	return client, nil
}
