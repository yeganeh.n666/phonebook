package pagination

import (
	"net/http"
	"strconv"
)

var (
	// DefaultPageSize specifies the default page size
	DefaultPageSize int64 = 10
	// MaxPageSize specifies the maximum page size
	MaxPageSize int64 = 50
	// PageVar specifies the query parameter name for page number
	PageVar = "page"
	// PageSizeVar specifies the query parameter name for page size
	PageSizeVar = "limit"
)

// Pages represents a paginated list of data items.
type Pages struct {
	Total     int64       `json:"total"`
	Page      int64       `json:"page"`
	PerPage   int64       `json:"perPage"`
	Prev      int64       `json:"prev"`
	Next      int64       `json:"next"`
	TotalPage int64       `json:"totalPage"`
	Docs      interface{} `json:"docs"`
}

// NewPage creates a new Pages instance.
// The page parameter is 1-based and refers to the current page index/number.
// The limit parameter refers to the number of items on each page.
func NewPage(page, limit int64) *Pages {
	if limit <= 0 {
		limit = DefaultPageSize
	}
	if limit > MaxPageSize {
		limit = MaxPageSize
	}

	if page < 1 {
		page = 1
	}
	return &Pages{
		Page:    page,
		PerPage: limit,
	}
}

// NewFromRequest creates a Pages object using the query parameters found in the given HTTP request.
func NewFromRequest(req *http.Request) *Pages {
	page := parseInt(req.URL.Query().Get(PageVar), 1)
	perPage := parseInt(req.URL.Query().Get(PageSizeVar), DefaultPageSize)
	return NewPage(page, perPage)
}

// parseInt parses a string into an integer. If parsing is failed, defaultValue will be returned.
func parseInt(value string, defaultValue int64) int64 {
	if value == "" {
		return defaultValue
	}
	if result, err := strconv.ParseInt(value, 10, 64); err == nil {
		return result
	}
	return defaultValue
}

// PageNum returns the page value that can be used in a MongoDb statement.
func (p *Pages) PageNum() int64 {
	return p.Page
}

// Limit returns the LIMIT value that can be used in a MongoDb statement.
func (p *Pages) Limit() int64 {
	return p.PerPage
}

func (p *Pages) AddInfo(info PaginationData) *Pages {
	p.Next = info.Next
	p.Prev = info.Prev
	p.Total = info.Total
	p.TotalPage = info.TotalPage
	return p
}
