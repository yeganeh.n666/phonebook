package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"phonebook/internal/config"
	"phonebook/internal/contact"
	"phonebook/internal/errors"
	"phonebook/internal/user"
	"phonebook/pkg/jwt"
	"phonebook/pkg/log"
	"phonebook/pkg/mongodb"
	"phonebook/pkg/redis"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
)

// Version indicates the current version of the application.
var Version = "1.0.0"
var flagConfig = flag.String("config", "./config/local.yml", "path to the config file")

func main() {
	flag.Parse()
	// create root logger tagged with server version
	logger := log.New().With(nil, "version", Version)
	var err error
	// load application configurations
	config.Cfg, err = config.Load(*flagConfig, logger)
	if err != nil {
		logger.Errorf("failed to load application configuration: %s", err)
		os.Exit(-1)
	}

	// connect to the database
	db, err := mongodb.ConnectToMongo(config.Cfg.MongoDB.DbName, config.Cfg.MongoDB.URL)
	if err != nil {
		logger.Error(err)
		os.Exit(-1)
	}

	redisClient, err := redis.ConnectToRedis(config.Cfg.Redis.Dsn)
	if err != nil {
		logger.Error(err)
		os.Exit(-1)
	}

	jwtService, err := jwt.New(jwt.Options{
		AccessSecret:  config.Cfg.JwtRSAKeys.Access,
		RefreshSecret: config.Cfg.JwtRSAKeys.Refresh,
		RedisClient:   redisClient,
	})
	if err != nil {
		logger.Error(err)
		os.Exit(-1)
	}

	// build HTTP server
	bindAddress := fmt.Sprintf(":%v", config.Cfg.ServerPort)

	// create a new server
	s := http.Server{
		Addr:         bindAddress,                                      // configure the bind address
		Handler:      buildHandler(logger, db, config.Cfg, jwtService), // set the default handler
		ReadTimeout:  5 * time.Second,                                  // max time to read request from the client
		WriteTimeout: 10 * time.Second,                                 // max time to write response to the client
		IdleTimeout:  120 * time.Second,                                // max time for connections using TCP Keep-Alive
	}

	// start the server
	go func() {
		logger.Infof("Starting server on port: %s", bindAddress)

		err := s.ListenAndServe()
		if err != nil {
			logger.Errorf("Error starting server: %s", err)
			os.Exit(1)
		}
	}()

	// trap sigterm or interrupt and gracefully shutdown the server
	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	// Block until a signal is received.
	sig := <-done
	logger.Infof("Got signal: %s", sig)

	// gracefully shutdown the server, waiting max 30 seconds for current operations to complete
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	if err := s.Shutdown(ctx); err != nil {
		logger.Errorf("Server Shutdown with Error: %s", err)
	} else {
		logger.Info("Server Shutdown gracefully")
	}
}

// buildHandler sets up the HTTP routing and builds an HTTP handler.
func buildHandler(logger log.Logger, db *mongo.Database, cfg *config.Config, jwtService *jwt.Auth) http.Handler {
	gin.SetMode(gin.ReleaseMode)
	router := gin.New()

	router.Use(
		errors.Handler(logger),
	)

	rg := router.Group("/api/v1")

	authHandler := jwtService.Handler()

	user.RegisterHandlers(rg.Group("/user"),
		user.NewService(user.NewRepository(db, logger), logger, jwtService),
		logger, authHandler,
	)

	rg.Use(jwtService.Handler())

	contact.RegisterHandlers(rg.Group("/contact"),
		contact.NewService(contact.NewRepository(db, logger), logger),
		logger,
	)

	return router
}
