module phonebook

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/go-ozzo/ozzo-routing v2.1.4+incompatible
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/go-playground/validator/v10 v10.4.1
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-redis/redis/v7 v7.4.0
	github.com/google/uuid v1.1.2
	github.com/pkg/errors v0.9.1
	github.com/qiangxue/go-env v1.0.1
	github.com/twinj/uuid v1.0.0
	go.mongodb.org/mongo-driver v1.4.4
	go.uber.org/zap v1.16.0
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	gopkg.in/yaml.v2 v2.4.0
)
