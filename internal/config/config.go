package config

import (
	"io/ioutil"
	"phonebook/pkg/log"

	"github.com/qiangxue/go-env"
	"gopkg.in/yaml.v2"
)

const (
	defaultServerPort = 8080
)

// Cfg is holder of config load file
var Cfg *Config

// Config represents an application configuration.
type Config struct {
	ServerPort int `yaml:"server_port" env:"SERVER_PORT"`

	MongoDB struct {
		DbName string `yaml:"db_name" env:"DB_NAME"`
		URL    string `yaml:"url" env:"DB_URL,secret"`
	} `yaml:"mongo_db"`

	Redis struct {
		Dsn string `yaml:"dsn" env:"DSN"`
	} `yaml:"redis"`

	JwtRSAKeys struct {
		Access  string `yaml:"access" env:"JWT_ACCESS_KEY"`
		Refresh string `yaml:"refresh" env:"JWT_REFRESH_KEY"`
	} `yaml:"jwt_rsa_keys"`
}

// Load returns an application configuration which is populated from the given configuration file and environment variables.
func Load(file string, logger log.Logger) (*Config, error) {
	// default config
	c := Config{
		ServerPort: defaultServerPort,
	}

	// load from YAML config file
	bytes, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	if err = yaml.Unmarshal(bytes, &c); err != nil {
		return nil, err
	}

	// load from environment variables prefixed with "APP_"
	if err = env.New("APP_", logger.Infof).Load(&c); err != nil {
		return nil, err
	}

	return &c, err
}
