package errors

import (
	"encoding/json"
	"errors"
	"fmt"
	"runtime/debug"

	"go.mongodb.org/mongo-driver/mongo"

	"net/http"

	"phonebook/pkg/log"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

// Handler creates a middleware that handles panics and errors encountered during HTTP request processing.
func Handler(logger log.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error
		defer func() {
			if e := c.Errors.Last(); e != nil {
				err = e.Err
			}
			l := logger.With(c.Request.Context())
			if e := recover(); e != nil {
				var ok bool
				if err, ok = e.(error); !ok {
					err = fmt.Errorf("%v", e)
				}

				l.Errorf("recovered from panic (%v): %s", err, debug.Stack())
			}
			if err != nil {
				res := buildErrorResponse(err)
				if res.StatusCode() == http.StatusInternalServerError {
					l.Errorf("encountered internal server error: %v", err)
				}
				c.JSON(res.Status, res)
				c.Abort() // skip any pending handlers since an error has occurred
				err = nil // return nil because the error is already handled
			}
		}()
		c.Next()
	}
}

// buildErrorResponse builds an error response from an error.
func buildErrorResponse(err error) ErrorResponse {
	switch err.(type) {
	case validator.ValidationErrors:
		return InvalidInput(err.(validator.ValidationErrors))
	case ErrorResponse:
		return err.(ErrorResponse)
	case *json.UnmarshalTypeError:
		return BadRequest("")
	}

	if errors.Is(err, mongo.ErrNoDocuments) {
		return NotFound("Document not found")
	}

	return InternalServerError("")
}
