package contact

import (
	"context"
	"errors"
	"phonebook/internal/entity"
	"phonebook/pkg/log"
	"phonebook/pkg/pagination"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

const (
	collectionName string = "contacts"
)

var (
	// ErrMatchingData you get this error if data not found.
	ErrMatchingData = errors.New("Data not found")
)

// Repository encapsulates the logic to access from the data source.
type Repository interface {
	Create(context.Context, *entity.Contact) error
	Get(ctx context.Context, page, limit int64) ([]entity.Contact, pagination.PaginationData, error)
	Update(context.Context, primitive.ObjectID, map[string]interface{}) error
	Delete(context.Context, primitive.ObjectID) error
}

// repository persists in database
type repository struct {
	db     *mongo.Database
	logger log.Logger
}

// NewRepository creates a new repository
func NewRepository(db *mongo.Database, logger log.Logger) Repository {
	return repository{db, logger}
}

// Create saves a new contact in the database.
func (r repository) Create(ctx context.Context, contact *entity.Contact) error {
	collection := r.db.Collection(collectionName)
	res, err := collection.InsertOne(ctx, contact)
	if err != nil {
		return err
	}
	contact.ID = res.InsertedID.(primitive.ObjectID)
	return nil
}

// Get returns and paginate user contacts.
func (r repository) Get(ctx context.Context, page, limit int64) ([]entity.Contact, pagination.PaginationData, error) {
	collection := r.db.Collection(collectionName)
	paginatedData, err := pagination.New(collection).Limit(limit).Page(page).Filter(bson.M{}).Find()
	if err != nil {
		return nil, pagination.PaginationData{}, err
	}
	var lists []entity.Contact
	for _, raw := range paginatedData.Data {
		var contact *entity.Contact
		if marshallErr := bson.Unmarshal(raw, &contact); marshallErr == nil {
			lists = append(lists, *contact)
		}
	}
	paginationInfo := paginatedData.Pagination
	return lists, paginationInfo, nil
}

// Update a contact by ObjectID.
func (r repository) Update(ctx context.Context, id primitive.ObjectID, contact map[string]interface{}) error {
	collection := r.db.Collection(collectionName)
	filter := bson.M{"_id": bson.M{"$eq": id}}
	update := bson.M{"$set": contact}
	result, err := collection.UpdateOne(ctx, filter, update)
	if result.MatchedCount == 0 {
		return ErrMatchingData
	}
	return err
}

// Delete a contact by ObjectID.
func (r repository) Delete(ctx context.Context, id primitive.ObjectID) error {
	collection := r.db.Collection(collectionName)
	_, err := collection.DeleteOne(ctx, bson.M{"_id": id})
	return err
}
