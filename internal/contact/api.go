package contact

import (
	"net/http"
	"phonebook/internal/entity"
	"phonebook/internal/errors"
	"phonebook/pkg/log"
	"phonebook/pkg/pagination"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// RegisterHandlers sets up the routing of the HTTP handlers.
func RegisterHandlers(r *gin.RouterGroup, service Service, logger log.Logger) {
	res := resource{service, logger}

	r.POST("/", res.create)
	r.GET("/", res.get)
	r.PUT("/:id", res.update)
	r.DELETE("/:id", res.delete)
}

const successfulResponse string = "Successful"

type resource struct {
	service Service
	logger  log.Logger
}

//read Contact from request body
//get userID from context by jwt
//create new contact
func (res resource) create(c *gin.Context) {
	contact := &entity.Contact{}
	if err := c.BindJSON(contact); err != nil {
		res.logger.With(c.Request.Context()).Info(err)
		c.Error(err)
		return
	}
	id, ok := c.Get("user_id")
	if !ok {
		c.Error(errors.BadRequest(""))
		return
	}
	contact.UserID, _ = id.(primitive.ObjectID)
	err := res.service.Create(c.Request.Context(), contact)
	if err != nil {
		c.Error(err)
		return
	}
	c.JSON(http.StatusCreated, contact)
}

//read page and limit value for pagination from Query param
//get user contacts
func (res resource) get(c *gin.Context) {
	ctx := c.Request.Context()
	pages := pagination.NewFromRequest(c.Request)
	phonebook, info, err := res.service.Get(ctx, pages.PageNum(), pages.Limit())
	if err != nil {
		c.Error(err)
		return
	}
	pages.Docs = phonebook
	c.JSON(http.StatusOK, pages.AddInfo(info))
}

//read updated values from request body
//get contact ObjectID from param
//update contact
func (res resource) update(c *gin.Context) {
	ctx := c.Request.Context()
	contact := make(map[string]interface{})
	if err := c.BindJSON(&contact); err != nil {
		res.logger.With(c.Request.Context()).Info(err)
		c.Error(err)
		return
	}
	if err := res.service.Update(ctx, c.Param("id"), contact); err != nil {
		c.Error(errors.BadRequest(err.Error()))
		return
	}
	c.JSON(http.StatusOK, contact)
}

//get contact ObjectID from param
//delete contact
func (res resource) delete(c *gin.Context) {
	ctx := c.Request.Context()
	if err := res.service.Delete(ctx, c.Param("id")); err != nil {
		res.logger.With(c.Request.Context()).Info(err)
		c.Error(err)
		return
	}
	c.JSON(http.StatusOK, successfulResponse)
}
