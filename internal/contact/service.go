package contact

import (
	"context"
	"errors"
	"phonebook/internal/entity"
	"phonebook/pkg/log"
	"phonebook/pkg/pagination"
	"phonebook/pkg/validators"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Service encapsulates usecase logic.
type Service interface {
	Create(context.Context, *entity.Contact) error
	Get(ctx context.Context, page, limit int64) ([]entity.Contact, pagination.PaginationData, error)
	Update(context.Context, string, map[string]interface{}) error
	Delete(context.Context, string) error
}

type service struct {
	repo   Repository
	logger log.Logger
}

// NewService creates a new service
func NewService(repo Repository, logger log.Logger) Service {
	return service{repo, logger}
}

//validate contact
//Create contact
func (s service) Create(ctx context.Context, contact *entity.Contact) error {
	if ok, err := validators.Validate(contact); !ok {
		return err
	}
	return s.repo.Create(ctx, contact)
}

//Get paginated contacts
func (s service) Get(ctx context.Context, page, limit int64) ([]entity.Contact, pagination.PaginationData, error) {
	contacts, info, err := s.repo.Get(ctx, page, limit)
	if err != nil {
		return []entity.Contact{}, pagination.PaginationData{}, err
	}
	return contacts, info, nil
}

//check contact id
//Update contact
func (s service) Update(ctx context.Context, id string, contact map[string]interface{}) error {
	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return errors.New("invalid ID")
	}
	return s.repo.Update(ctx, objID, contact)
}

//check contact id
//Delete contact
func (s service) Delete(ctx context.Context, id string) error {
	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return errors.New("invalid ID")
	}
	return s.repo.Delete(ctx, objID)
}
