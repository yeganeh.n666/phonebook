package entity

import "go.mongodb.org/mongo-driver/bson/primitive"

type Contact struct {
	ID           primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	UserID       primitive.ObjectID `json:"userId" bson:"userId"`
	Name         string             `json:"name" bson:"name" validate:"required"`
	PhoneNumbers []string           `json:"phoneNumbers" bson:"phoneNumbers" validate:"required"`
	Description  string             `json:"description" bson:"description"`
}

type UpdateContactRequest struct {
}
