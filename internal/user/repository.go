package user

import (
	"context"
	"errors"
	"phonebook/internal/entity"
	"phonebook/pkg/log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

const (
	collectionName string = "users"
)

var (
	// ErrProcess you get this error while converting ctx or interface and mongo error.
	ErrProcess = errors.New("Error while processing your request")
	// ErrMatchingData you get this error if data not found.
	ErrMatchingData = errors.New("Data not found")
)

// Repository encapsulates the logic to access from the data source.
type Repository interface {
	// Create saves a new record in the storage.
	Create(context.Context, *entity.User) error
	FindOneByEmail(context.Context, string) (*entity.User, error)
}

// repository persists in database
type repository struct {
	db     *mongo.Database
	logger log.Logger
}

// NewRepository creates a new repository
func NewRepository(db *mongo.Database, logger log.Logger) Repository {
	return repository{db, logger}
}

// Create saves a new user in the database.
func (r repository) Create(ctx context.Context, user *entity.User) error {
	collection := r.db.Collection(collectionName)
	res, err := collection.InsertOne(ctx, user)
	if err != nil {
		return err
	}
	user.ID = res.InsertedID.(primitive.ObjectID)
	return nil
}

func (r repository) FindOneByEmail(ctx context.Context, email string) (*entity.User, error) {
	collection := r.db.Collection(collectionName)
	user := &entity.User{}
	err := collection.FindOne(ctx, bson.D{{Key: "email", Value: email}}).Decode(user)
	if err != nil {
		return nil, err
	}
	return user, nil
}
