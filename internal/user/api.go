package user

import (
	"net/http"
	"phonebook/pkg/log"

	"github.com/gin-gonic/gin"
)

// RegisterHandlers sets up the routing of the HTTP handlers.
func RegisterHandlers(r *gin.RouterGroup, service Service, logger log.Logger, authHandler gin.HandlerFunc) {
	res := resource{service, logger}
	r.POST("/login", res.login)
	r.POST("/logout", res.logout)
	r.POST("/refresh", res.refresh)
	r.POST("/register", res.register)
}

const successfulResponse string = "Successful"

type resource struct {
	service Service
	logger  log.Logger
}

//get email and password from request body
//login user and return tokens
func (res resource) login(c *gin.Context) {
	input := AuthUserByEmailRequest{}
	if err := c.BindJSON(&input); err != nil {
		res.logger.With(c.Request.Context()).Info("email", err)
		c.Error(err)
		return
	}
	tokens, err := res.service.Login(c.Request.Context(), input)
	if err != nil {
		c.Error(err)
		return
	}
	c.JSON(http.StatusOK, tokens)
}

//get refreshToken from request body
//refresh and return tokens
func (res resource) refresh(c *gin.Context) {
	input := InputToken{}
	if err := c.BindJSON(&input); err != nil {
		res.logger.With(c.Request.Context()).Info(err)
		c.Error(err)
		return
	}
	tokens, err := res.service.Refresh(c.Request.Context(), input)
	if err != nil {
		c.Error(err)
		return
	}
	c.JSON(http.StatusOK, tokens)
}

//logout user
func (res resource) logout(c *gin.Context) {
	err := res.service.Logout(c.Request)
	if err != nil {
		c.Error(err)
		return
	}
	c.JSON(http.StatusOK, successfulResponse)
}

//get email and password from request body
//register user and return tokens
func (res resource) register(c *gin.Context) {
	input := AuthUserByEmailRequest{}
	if err := c.BindJSON(&input); err != nil {
		res.logger.With(c.Request.Context()).Info(err)
		c.Error(err)
		return
	}
	response, err := res.service.RegisterWithEmail(c.Request.Context(), input)
	if err != nil {
		c.Error(err)
		return
	}
	c.JSON(http.StatusCreated, response)
}
