package user

import (
	"context"
	"net/http"
	"phonebook/internal/entity"
	"phonebook/internal/errors"
	"phonebook/pkg/jwt"
	"phonebook/pkg/log"
	"phonebook/pkg/validators"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
)

var (
	defaultRole = "user"
)

// Service encapsulates usecase logic.
type Service interface {
	Create(context.Context, *entity.User) error
	Login(context.Context, AuthUserByEmailRequest) (*Tokens, error)
	Refresh(context.Context, InputToken) (*Tokens, error)
	Logout(*http.Request) error
	RegisterWithEmail(context.Context, AuthUserByEmailRequest) (*RegisterResponse, error)
}

type AuthUserByEmailRequest struct {
	Email    string `json:"email" bson:"email" validate:"required,email"`
	Password string `json:"password" bson:"password" validate:"required,password"`
}

type InputToken struct {
	Token string `json:"refreshToken" validate:"required"`
}

type RegisterResponse struct {
	entity.User
	Tokens
}

type Tokens struct {
	AccessToken  string `json:"accessToken"`
	RefreshToken string `json:"refreshToken"`
}

type service struct {
	repo   Repository
	logger log.Logger
	jwt    *jwt.Auth
}

func (s service) Create(ctx context.Context, user *entity.User) error {
	if ok, err := validators.Validate(user); !ok {
		return err
	}
	return s.repo.Create(ctx, user)
}

// NewService creates a new service.
func NewService(repo Repository, logger log.Logger, jwt *jwt.Auth) Service {
	return service{repo, logger, jwt}
}

func (s service) Login(ctx context.Context, req AuthUserByEmailRequest) (*Tokens, error) {
	if ok, err := validators.Validate(req); !ok {
		return nil, err
	}
	user, err := s.authenticate(ctx, req.Email, req.Password)
	if err != nil {
		return nil, err
	}
	jwtTokens, err := s.generateJWT(user.ID)
	if err != nil {
		return nil, err
	}
	return jwtTokens, nil
}

//validate tokens
//Refresh tokens
func (s service) Refresh(ctx context.Context, req InputToken) (*Tokens, error) {
	if ok, err := validators.Validate(req); !ok {
		return nil, err
	}
	ts, err := s.jwt.RefreshToken(req.Token)
	if err != nil {
		return nil, err
	}
	return &Tokens{
		AccessToken:  ts.AccessToken,
		RefreshToken: ts.RefreshToken,
	}, nil
}

//extract token metadata from request
//Logout and DeleteTokens
func (s service) Logout(req *http.Request) error {
	metadata, err := s.jwt.ExtractTokenMetadata(req)
	if err != nil {
		return err
	}
	if err := s.jwt.DeleteTokens(metadata); err != nil {
		return err
	}
	return nil
}

//validate request
//check email and hash password
//generateJWT
//RegisterWithEmail and return user and tokens
func (s service) RegisterWithEmail(ctx context.Context, req AuthUserByEmailRequest) (*RegisterResponse, error) {
	if ok, err := validators.Validate(req); !ok {
		return nil, err
	}
	user, err := s.checkAndCreate(ctx, req)
	if err != nil {
		return nil, err
	}
	jwtTokens, err := s.generateJWT(user.ID)
	if err != nil {
		return nil, err
	}
	res := &RegisterResponse{}
	res.User = *user
	res.Tokens = *jwtTokens

	return res, nil
}

// authenticate authenticates a user using username and password.
// If username and password are correct, an user is returned.
func (s service) authenticate(ctx context.Context, email, password string) (*entity.User, error) {
	user, err := s.repo.FindOneByEmail(ctx, email)
	if err != nil {
		return nil, errors.Unauthorized("invalid data")
	}
	if bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)) == nil {
		return user, nil
	}
	return nil, errors.Unauthorized("invalid data")
}

// generateJWT generates a JWT that encodes an identity.
func (s service) generateJWT(userID primitive.ObjectID) (*Tokens, error) {
	ts, err := s.jwt.CreateToken(userID, defaultRole)
	if err != nil {
		return nil, err
	}
	if err := s.jwt.CreateAuth(userID, ts); err != nil {
		return nil, err
	}
	return &Tokens{
		AccessToken:  ts.AccessToken,
		RefreshToken: ts.RefreshToken,
	}, nil
}

//checkAndCreate check email in database
//if email not found hash password and create new user
func (s service) checkAndCreate(ctx context.Context, req AuthUserByEmailRequest) (*entity.User, error) {
	_, err := s.repo.FindOneByEmail(ctx, req.Email)
	if err == nil {
		return nil, errors.BadRequest("You have already registered")
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(req.Password), 10)
	if err != nil {
		return nil, err
	}

	user := &entity.User{
		Email:    req.Email,
		Password: string(hashedPassword),
	}

	err = s.repo.Create(ctx, user)
	if err != nil {
		return nil, err
	}
	return user, nil
}
